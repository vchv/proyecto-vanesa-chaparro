var accountsMock = [
  {
    "id": "2002",
    "name": "Cuenta personal C",
    "description": {
      "value": "050400010100001604",
      "masked": true
    },
    "primaryAmount": {
      "amount": 70000,
      "currency": "CLP",
      "label": "Available"
    },
    "secondaryAmount": {
      "amount": 70000,
      "currency": "CLP",
      "label": "Available"
    },
    "scale": 0,
    "imgSrc": "",
    "additionalDescription": "any"
  }
];

var accountsBalanceMock = {
  "amount": 3782000,
  "currency": "CLP",
  "scale": 0
};
