![cells-molecule-header-dropdown screenshot](cells-molecule-header-dropdown.png)

# cells-molecule-header-dropdown

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-molecule-header-dropdown)

`<cells-molecule-header-dropdown>` displays a layer with three content areas when the
expand icon is clicked.

It can receive the properties: condensedShape, condensedColor, expandedShape and expandedColor.

## Example

Using with properties and content:

```html
<cells-molecule-header-dropdown
    condensed-shape="B34"
    condensed-color="#888"
    expanded-shape="B40"
    expanded-color="#ddd">
  <div class="header-dropdown-sidehead">
    <b>This is the sidehead</b>
  </div>
  <div class="header-dropdown-content"><b>This is the content</b><br> Pellentesque habitant morbi
      tristique senectus et netus et malesuada fames ac turpis egestas.
   </div>
   <div class="header-dropdown-footer"><b>This is the footer</b><br> Pellentesque habitant morbi
     tristique senectus et netus et malesuada fames ac turpis egestas.
  </div>
</cells-molecule-header-dropdown>
```

## Content:

The `<cells-molecule-header-dropdown>` have three layout areas where you can add content.
You have to apply the classes into the content to add it into the correct area.

| Area                     | Description              |
|:-------------------------|:-------------------------|
| header-dropdown-sidehead | Left header side area    |
| header-dropdown-content  | Main center content area |
| header-dropdown-footer   | Content in bottom area   |


## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                                       | Selector                    | CSS Property | Value                                                    |
| ----------------------------------------------------- | --------------------------- | ------------ | -------------------------------------------------------- |
| --cells-molecule-header-dropdown-background-condensed | :host                       | background   |  transparent                                             |
| --cells-molecule-header-dropdown-height-condensed     | :host                       | height       |  120px                                                   |
| --cells-molecule-header-dropdown-background-z-index   | :host                       | z-index      |  1                                                       |
| --cells-fontDefault                                   | :host                       | font-family  |  sans-serif                                              |
| --cells-molecule-header-dropdown-background-expanded  | :host::before               | background   |  --gradient                                              |
| --cells-molecule-header-dropdown-icon-color           | .header-dropdown-iconbutton | color        |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff |
| --cells-molecule-header-dropdown-icon-trasition       | .header-dropdown-iconbutton | transition   |  --transition                                            |
| --cells-molecule-header-dropdown-height-expanded      | :host([expanded])           | height       |  100%                                                    |
### @apply
| Mixins                                        | Selector                                            | Value |
| --------------------------------------------- | --------------------------------------------------- | ----- |
| --cells-molecule-header-dropdown              | :host                                               | {}    |
| --cells-molecule-header-dropdown-content-all  | header > ::slotted(*), header .header-dropdown-head | {}    |
| --cells-molecule-header-dropdown-content-head | header .header-dropdown-head                        | {}    |
| --content-header-dropdown-iconbutton          | header .header-dropdown-iconbutton                  | {}    |
| --layout-horizontal-reverse                   | :host([reverse]) header                             | {}    |
