(function() {
  'use strict';
  Polymer({
    is: 'cells-molecule-header-dropdown',

    hostAttributes: {
      'tabindex': '-1'
    },

    properties: {

      /**
       * Indicates if header is expanded
       */
      expanded: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /**
       * Computed property to set color depending on if header is expanded or not
       * @type {String}
       */
      color: {
        type: String,
        computed: '_setColor(expanded, expandedColor, condensedColor)',
        observer: '_changeIconColor'
      },

      /**
       * Computed property to set shape depending on if header is expanded or not
       * @type {String}
       */
      shape: {
        type: String,
        computed: '_setShape(expanded, expandedShape, condensedShape)'
      },

      /**
       * Shape if header is not extended
       * @type {String}
       */
      condensedShape: {
        type: String,
        value: 'banking:B34'
      },

      /**
       * Color if header is not extended
       * @type {String}
       */
      condensedColor: {
        type: String,
        value: '#224fbd'
      },

      /**
       * Shape if header is extended
       * @type {String}
       */
      expandedShape: {
        type: String,
        value: 'banking:B40'
      },

      /**
       * Color if header is not extended
       * @type {String}
       */
      expandedColor: {
        type: String,
        value: '#fff'
      },

      /**
       * @type {Boolean}
       */
      reverse: {
        type: Boolean,
        reflectToAttribute: true
      }
    },

    listeners: {
      'transitionend': '_transitionEnd',
      'wheel': '_cancelScroll',
      'keydown': '_keydownHandler',
      'tap': 'close',
      'header-dropdown-iconbutton.keydown': '_enterHandler'
    },

    /**
     * Changes the expanded property to false
     * @return {void}
     */
    close: function() {
      this.expanded = false;
    },

    /**
     * Changes the expanded property to true
     * @return {void}
     */
    open: function() {
      this.expanded = true;
    },

    /**
     * Calls close or open functions depending on the expanded property value
     */
    toggleHeader: function(e) {
      if (e) {
        e.stopPropagation();
      }
      return (this.expanded) ? this.close() : this.open();
    },

    /**
     * cancel the page scroll
     * @return {void}
     */
    _cancelScroll: function(e) {
      e.stopPropagation();
    },

    /**
     * change the icon color
     * @return {void}
     */
    _changeIconColor: function() {
      this.updateStyles({'--cells-molecule-header-dropdown-icon-color': this.color});
    },

    /**
     * calls to toggleHeader if escape key
     * @return {void}
     */
    _keydownHandler: function(e) {
      if (e.keyCode === 27 && this.expanded) {
        this.toggleHeader();
      }
    },

    /**
     * calls to toggleHeader if enter key
     * @return {void}
     */
    _enterHandler: function(e) {
      if (e.keyCode === 13) {
        this.toggleHeader();
      }
    },

    /**
     * set color to icon
     * @param {Boolean} expanded true if header is expanded
     * @param {String} expandedColor Color to set icon when expanded is true
     * @param {String} condensedColor Color to set icon when expanded is false
     */
    _setColor: function(expanded, expandedColor, condensedColor) {
      return (expanded) ? expandedColor : condensedColor;
    },

    /**
     * set shape to icon
     * @param {Boolean} expanded true if header is expanded
     * @param {String} expandedShape Shape to set icon when expanded is true
     * @param {String} condensedShape Shape to set icon when expanded is false
     */
    _setShape: function(expanded, expandedShape, condensedShape) {
      return (expanded) ? expandedShape : condensedShape;
    },

    /**
     * fire the event to finish the transition
     */
    _transitionEnd: function(e) {
      if (e.target.localName === 'cells-molecule-header-dropdown') {
        if ((e.propertyName === 'height' || e.propertyName === 'opacity') && !this.expanded) {
          /**
           * fired when the dropdown is condensed
           * @event header-dropdown-condensed
           */
          this.dispatchEvent(new CustomEvent('header-dropdown-condensed', {
            bubbles: true,
            composed: true
          }));
        } else {
          if ((e.propertyName === 'height' || e.propertyName === 'opacity') && this.expanded) {
            /**
             * fired when the dropdown is expanded
             * @event header-dropdown-expanded
             */
            this.dispatchEvent(new CustomEvent('header-dropdown-expanded', {
              bubbles: true,
              composed: true
            }));
          }
        }
      } else {
        e.stopPropagation();
      }
    }
  });
}());
