var mainItems = [{
  "key": "Saldo disponible",
  "value": [{
    "amount": 3500000.20,
    "currency": "EUR"
  }, {
    "amount": 1070734.45,
    "currency": "EUR"
  }]
}, {
  "key": "Saldo contable",
  "value": [{
    "amount": 456,
    "currency": "EUR"
  }, {
    "amount": 654,
    "currency": "EUR"
  }]
}];
var optionalItems = [{
    "key": "Deuda total",
    "value": {
      "amount": 775345,
      "currency": "EUR"
    }
  }, {
    "key": "Cupo total",
    "value": {
      "amount": 534500,
      "currency": "EUR"
    }
  }];

var additionalItems = [{
  "key": "Teléfono asociado",
  "value": 666111222,
  "masked": true
}, {
  "key": "Cuenta asociada",
  "value": 9876543210
}];

var statusItem = {
  'status': {
    'id': 'CANCELED',
    'name': 'Cerrada por fusión de sucursales',
    'class': 'canceled',
    'message': 'cells-product-summary-account-status-canceled-message',
    'icon': 'coronita:block'    
  }
};