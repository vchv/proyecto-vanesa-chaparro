class MySecondComponent extends Polymer.Element {

  static get is() {
    return 'my-second-component';
  }

  static get properties() {
    return {
    title:{
      type: String,
      notify: true
          }
  };
}
  _handleClick() {
    this.dispatchEvent(new CustomEvent('message-send', {bubles: true, composed: true}))
  }

}


customElements.define(MySecondComponent.is, MySecondComponent);
