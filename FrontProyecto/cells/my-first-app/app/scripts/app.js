(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'home': '/',
      'another': '/another',
      'users': '/users'
    }
  });

}(document));
